package ru.t1.akolobov.tm.logger;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.logger.api.IPropertyService;
import ru.t1.akolobov.tm.logger.listener.EntityListener;
import ru.t1.akolobov.tm.logger.service.LoggerService;
import ru.t1.akolobov.tm.logger.service.PropertyService;

import javax.jms.*;

public class ConsumerApplication {

    public static void main(String[] args) throws Exception {

        IPropertyService propertyService = new PropertyService();

        @NotNull final String url = propertyService.getConsumerUrl();
        @NotNull final String queue = propertyService.getConsumerQueue();
        final boolean isServerEnabled = Boolean.parseBoolean(propertyService.getServerEnabled());

        if(isServerEnabled) {
            BrokerService brokerService = new BrokerService();
            brokerService.addConnector(url);
            brokerService.start();
        }

        @NotNull final LoggerService loggerService = new LoggerService(propertyService);
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);

        @NotNull final ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(url);
        @NotNull final Connection connection = connectionFactory.createConnection();
        @NotNull final Session session;
        @NotNull final Queue destination;

        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(queue);
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(entityListener);
    }

}
