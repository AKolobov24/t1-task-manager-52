package ru.t1.akolobov.tm.logger.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoDatabase database;

    public LoggerService(@NotNull IPropertyService propertyService) {
        @NotNull MongoClient mongoClient = new MongoClient(
                propertyService.getDatabaseHost(),
                Integer.parseInt(propertyService.getDatabasePort())
        );
        database = mongoClient.getDatabase(propertyService.getDatabaseName());
    }

    @SneakyThrows
    public void log(@NotNull String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = database.getCollection(collectionName);
        collection.insertOne(new Document(event));
        System.out.println(message);
    }

}
