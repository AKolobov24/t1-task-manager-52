package ru.t1.akolobov.tm.logger.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.logger.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class EntityListener implements MessageListener {

    @NotNull
    private final LoggerService loggerService;

    public EntityListener(@NotNull final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (message instanceof TextMessage) {
            @NotNull final TextMessage textMessage = (TextMessage) message;
            loggerService.log(textMessage.getText());
        }
    }

}
