package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.TaskDto;

@Getter
@Setter
@NoArgsConstructor
public class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable TaskDto task) {
        super(task);
    }

}
