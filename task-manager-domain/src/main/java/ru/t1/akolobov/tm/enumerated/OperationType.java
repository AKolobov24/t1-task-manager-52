package ru.t1.akolobov.tm.enumerated;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
